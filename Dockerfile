FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD target/spring-petclinic-2.4.5.jar app.jar
CMD  ["npm","run","start"]
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
